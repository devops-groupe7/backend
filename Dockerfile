# Utiliser une image Python officielle en tant que base
FROM python:3.12.0
# USER daemon:
# Définir le répertoire de travail dans le conteneur
WORKDIR /app

COPY requirements/base.txt requirements.txt


# COPY start.sh start.sh
# RUN pip install --upgrade pip
# Installer les dépendances
RUN pip install -r requirements.txt

# Copier les fichiers requis dans le conteneur
COPY . .
# Installer dos2unix pour convertir les fins de ligne du script
# RUN apt-get update && apt-get install -y dos2unix


# Rendre le script exécutable
RUN chmod +x ./start.sh

# Convertir le script start.sh à l'intérieur du conteneur
# RUN sed $'s/\r$//' ./start.sh > ./start.Unix.sh


# RUN chmod +x ./start.sh
CMD ["./start.sh"]