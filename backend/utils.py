import os
from dotenv import load_dotenv

# Charger les variables d'environnement du fichier .env
load_dotenv()

def get_required_environment_variable(name: str) -> str:
    if name not in os.environ:
        raise Exception(f"The variable '{name}' was not defined and is required for the application to start")
    return os.environ.get(name)