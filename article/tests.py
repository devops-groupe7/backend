from django.test import TestCase
# test_calculator.py

# Charger les variables d'environnement à partir du fichier .env lors des tests
# from dotenv import load_dotenv

# Charger les variables d'environnement du fichier .env
# load_dotenv()
# calculator.py

def add(x, y):
    return x + y



class UtilsTestCase(TestCase):

    def test_add_function(self):
        result = add(3, 4)
        self.assertEqual(result, 7)
    
    def test_add_four(self):
        result = add(-1, 5)
        self.assertEqual(result, 4)
        
    def test_add_zero(self):
        result = add(0, 0)
        self.assertEqual(result, 0)

# Create your tests here.